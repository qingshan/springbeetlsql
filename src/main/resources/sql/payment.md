queryUser
===

* 查询用户收入或者支出

	select pay.*,p.name park_name,pl.name place_name from payment pay left join park p on pay.park_id=p.id 
	left join place pl on p.id = pl.park_id
	where 1=1
	@if(isNotEmpty(userId)){
	and pay.user_id=#userId#
	@}
	@if(isNotEmpty(ownerId)){
	and pay.owner_id=#ownerId#
	@}
