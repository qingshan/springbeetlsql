package com.park.oss.vo;

import org.beetl.sql.core.annotatoin.Table;

import com.park.oss.model.SysUser;

public class UserOwner extends SysUser {
	private String plateName;
	private String placeName;
	public String getPlateName() {
		return plateName;
	}
	public void setPlateName(String plateName) {
		this.plateName = plateName;
	}
	public String getPlaceName() {
		return placeName;
	}
	public void setPlaceName(String placeName) {
		this.placeName = placeName;
	}
	
	
}
