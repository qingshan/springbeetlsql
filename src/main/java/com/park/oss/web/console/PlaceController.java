package com.park.oss.web.console;

import java.sql.Timestamp;
import java.util.Date;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.ModelAndView;

import com.park.oss.model.CarPlace;
import com.park.oss.model.Park;
import com.park.oss.service.PlaceService;
import com.park.oss.vo.PlaceVo;

@Controller
@RequestMapping("/place")
public class PlaceController {
	@Autowired
	PlaceService service ;
    @RequestMapping(value = "/index.html", method = RequestMethod.GET)
    public ModelAndView index(@RequestParam(value="parkId", required=false) Integer parkId) {
    	ModelAndView view = new ModelAndView("/place/index.btl");
    	List<PlaceVo> list = service.query(parkId);
    	view.addObject("list",list);
        return view;
    }
    
   
    
    
    @RequestMapping(value = "/order.html", method = RequestMethod.GET)
    public @ResponseBody String order(@RequestParam(value="parkId", required=true) Integer parkId,
    		@RequestParam(value="placeId", required=true) Integer placeId,
    		@RequestParam(value="mobile", required=true) String mobile,
    		@RequestParam(value="plate", required=true) String plate
    	
    		
    		) {
    	CarPlace place = new CarPlace();
    	place.setCreateTime(new Timestamp(new Date().getTime()));
    	place.setMobile(mobile);
    	place.setPlaceId(placeId);
    	place.setParkId(parkId);
    	place.setPlate(plate);
    	place.setStatus(0);
    Integer id =	service.order(place);
    return id.toString();
   }
    
    
    @RequestMapping(value = "/enter.html", method = RequestMethod.GET)
    public @ResponseBody String enter(
    		@RequestParam(value="id", required=true) Integer id
    		) {
    service.enter(id);
    return "success";
   }
    
    
    @RequestMapping(value = "/pay.html", method = RequestMethod.GET)
    public @ResponseBody String pay(
    		@RequestParam(value="id", required=true) Integer id
    		) {
    service.pay(id);
    return "success";
   }
    
    
    @RequestMapping(value = "/disable.html", method = RequestMethod.GET)
    public @ResponseBody String disable(
    		@RequestParam(value="id", required=true) Integer id
    		) {
    service.disableOrder(id);
    return "success";
   }
    
    
    @RequestMapping(value = "/exit.html", method = RequestMethod.GET)
    public @ResponseBody String exit(
    		@RequestParam(value="id", required=true) Integer id
    		) {
    service.exit(id);
    return "success";
   }
    
}
