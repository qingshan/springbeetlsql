package com.park.oss.test;

import org.beetl.sql.core.ClasspathLoader;
import org.beetl.sql.core.Interceptor;
import org.beetl.sql.core.SQLLoader;
import org.beetl.sql.core.SQLManager;
import org.beetl.sql.core.UnderlinedNameConversion;
import org.beetl.sql.core.db.MySqlStyle;
import org.beetl.sql.ext.DebugInterceptor;

import com.park.oss.model.CarPlace;

public class QuickTest {

	public static void main(String[] args) throws Exception{
		MySqlStyle style = new MySqlStyle();
		DBConnectoinSource cs = new DBConnectoinSource();
		SQLLoader loader = new ClasspathLoader("/sql");
		SQLManager sql = new SQLManager(style,loader,cs,new UnderlinedNameConversion(), new Interceptor[]{new DebugInterceptor()});
		
		
		CarPlace cp =new CarPlace();
		cp.setId(5);
		cp.setStatus(4);
		sql.updateTemplateById(cp);
		
		
//		sql.unique(CarPlace.class, 1);
		
		sql.genSQLTemplateToConsole("gate");
		sql.genPojoCodeToConsole("gate");
		
		
		
	


	}
}
