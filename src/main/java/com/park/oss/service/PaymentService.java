package com.park.oss.service;

import java.util.List;

import com.park.oss.model.CarPlace;
import com.park.oss.model.Payment;
import com.park.oss.model.SysUser;

public interface PaymentService {
	List<Payment> query(Integer parkId);
	List<Payment> queryUser(Integer userId,boolean isUser);
	
	CarPlace getAvaibleRent(Integer userId);
	
	int doRent(SysUser user,Integer parkId);
	void unRent(CarPlace cp,SysUser user,Integer parkId);
	
	void doEnter(SysUser user,Integer parkId);
	
	CarPlace doPay(SysUser user,Integer parkId);
	void doExit(SysUser user,Integer parkId);
}
