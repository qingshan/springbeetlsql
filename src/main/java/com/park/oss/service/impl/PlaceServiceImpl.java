package com.park.oss.service.impl;

import java.sql.Timestamp;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.beetl.sql.core.SQLManager;
import org.beetl.sql.core.SQLReady;
import org.beetl.sql.core.db.KeyHolder;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.park.oss.model.CarPlace;
import com.park.oss.model.Park;
import com.park.oss.model.Payment;
import com.park.oss.model.Place;
import com.park.oss.model.PlaceRent;
import com.park.oss.model.SysUser;
import com.park.oss.model.UserBalance;
import com.park.oss.service.PlaceService;
import com.park.oss.vo.PlaceVo;

@Service
@Transactional
public class PlaceServiceImpl implements PlaceService {

	@Autowired
	SQLManager sqlManager ;
	@Override
	public List<PlaceVo> query(Integer parkId) {
		Place query = new Place();
		query.setParkId(parkId);
		List<PlaceVo> list =sqlManager.select("place.select", PlaceVo.class, query,1,10);
		return list;
	}
	@Override
	public Integer order(CarPlace car) {
		KeyHolder holder = new KeyHolder();
		sqlManager.insert(CarPlace.class,car,holder);
		return holder.getInt();
	}
	@Override
	public void disableOrder(Integer id) {
		CarPlace place = new CarPlace();
		place.setStatus(4);
		place.setId(id);
		
		sqlManager.updateTemplateById(place);
		
	}
	@Override
	public void enter(Integer id) {
		CarPlace place = new CarPlace();
		place.setStatus(1);
		place.setId(id);
		place.setStartTime(new Timestamp(System.currentTimeMillis()));
		sqlManager.updateTemplateById(place);
		
	}
	@Override
	@Transactional
	public void pay(Integer id) {
		CarPlace order =sqlManager.unique(CarPlace.class, id);
		Integer parkId = order.getParkId();
		
		
		Park park =sqlManager.unique(Park.class, parkId);
		Integer price = park.getPrice();
		//算钱
		double money = getMoney(price,order.getStartTime(),new Date());
		
		//检查用户账户
		
		UserBalance ub = new UserBalance();
		ub.setUserId(order.getUserId());
		
		ub = sqlManager.template(ub).get(0);
		if(ub.getBalance()<money){
			throw new RuntimeException ("余额账户金额不够，请充值");
		}
		
		ub.setBalance(ub.getBalance()-money);
		ub.setUpdateTime(new Timestamp(new Date().getTime()));
		//扣除金额
		sqlManager.updateById(ub);
		
		int total = park.getOwnerRate()+park.getParkRate()+park.getPlatformRate();
		
		//支付
		Payment pay = new Payment();
		pay.setAmount(money);
		pay.setCarPlaceId(order.getId());
		pay.setCreateTime(new Timestamp(new Date().getTime()));
		pay.setMobile(order.getMobile());
		pay.setParkId(order.getParkId());
		pay.setPlate(order.getPlate());
		pay.setUserId(order.getUserId());
		pay.setOwnerId(order.getOwnerId());
		pay.setPlatformMoney(money*1.0*park.getPlatformRate()/total);
		pay.setOwnerMoney(money*1.0*park.getOwnerRate()/total);
		pay.setParkMoney(money*1.0*park.getParkRate()/total);
		pay.setPlaceId(order.getPlaceId());
		KeyHolder payHolder = new KeyHolder();
		sqlManager.insert(Payment.class,pay,payHolder);
		
		//更新订单
		CarPlace place = new CarPlace();
		place.setStatus(2);
		place.setId(id);
		place.setPaymentId(payHolder.getInt());
		place.setPayTime(pay.getCreateTime());
		sqlManager.updateTemplateById(place);
		
	}
	@Override
	public void exit(Integer id) {
		//更新订单
		
		
		
		CarPlace place = new CarPlace();
		place.setStatus(3);
		place.setId(id);
		place.setEndTime(new Timestamp(System.currentTimeMillis()));
		sqlManager.updateTemplateById(place);
		
		place = sqlManager.unique(CarPlace.class, id);
		
		//车位已用
		//演示直接用jdbc sql
		sqlManager.executeUpdate(new SQLReady("update place_rent set status=0 where id =?",place.getPalceRentId()));
		
	}
	
	
	@Override
	public List<PlaceVo> getUserPlace(Integer userId) {
		Map paras = new HashMap();
		paras.put("userId", userId);
		return sqlManager.select("place.selectUserPark", PlaceVo.class, paras);

	}
	
	 public static double getMoney(Integer price,Date enter,Date exit){
	       double time =  exit.getTime()-enter.getTime();
	        //换小时
	        time = time/(1000*60*60);
	        return time*price;
	    }
	@Override
	public void addPlace(Place place) {
		sqlManager.insert(place,true);
		// 车场增加一个
		Park park =sqlManager.unique(Park.class, place.getParkId());
		park.setFreeAmount(park.getFreeAmount()+1);
		sqlManager.updateById(park);	
		
		Integer userId =  place.getUserId();
		SysUser user =sqlManager.unique(SysUser.class, userId);
		user.setHasPlace(1);
		sqlManager.updateById(user);
		
	}
	@Override
	public Double getPayMoney(SysUser user,Integer parkId) {
		
		SQLManager sql = sqlManager;
		CarPlace query = new CarPlace();
		query.setParkId(parkId);
		query.setUserId(user.getId());
		query.setStatus(1);
		List<CarPlace> carPlaces = sql.template(query);
		CarPlace pl = carPlaces.get(0);
		
		
		CarPlace order =sqlManager.unique(CarPlace.class, pl.getId());
		
		
		
		Park park =sqlManager.unique(Park.class, parkId);
		Integer price = park.getPrice();
		//算钱
		double money = getMoney(price,order.getStartTime(),new Date());
		return money;
		
	}

}
